import java.util.Scanner;
public class VirtualPetApp{

	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);

		Horse []  arras = new Horse [1];
		
		for(int index = 0 ; index<arras.length; index++){
			
			System.out.println("Enter the type of the horse");
			String type = reader.nextLine();
			
			System.out.println(" If your horse is a racer horse enter true ");
			boolean racer = Boolean.parseBoolean(reader.nextLine());
			
			System.out.println(" What is the maximum life span of your horse? ");
			int maxLifeSpan = Integer.parseInt(reader.nextLine());
			
			//object
			arras[index] = new Horse(type, racer, maxLifeSpan);
			
		}
		System.out.println("BEFORE VALUES OF THE LAST ANIMAL IN THE ARRAY:");
		System.out.println("type: " + arras[arras.length-1].getType());
		System.out.println("racer: (t or f) "  + arras[arras.length-1].getRacer());
		System.out.println("maxLifeSpan: " +  arras[arras.length-1].getMaxLifeSpan());
		
		System.out.println("~~~~~~~~~~~~~~~~~");
		
		System.out.println("Input a new value (int) to change the lifespan");
		int changedValue = Integer.parseInt(reader.nextLine());
		arras[arras.length-1].setMaxLifeSpan(changedValue);
		
		System.out.println("~~~~~~~~~~~~~~~~~");
		System.out.println("AFTER VALUES OF THE LAST ANIMAL IN THE ARRAY:");
		System.out.println("type: " + arras[arras.length-1].getType());
		System.out.println("racer: (t or f) "  + arras[arras.length-1].getRacer());
		System.out.println("maxLifeSpan: " +  arras[arras.length-1].getMaxLifeSpan());
		
	}


} 









