public class Horse {

	private String type;
	private boolean racer;
	private int maxLifeSpan;	
	
	//constructor
	public Horse(String type, boolean racer, int maxLifeSpan){
		this.type = type;
		this.racer = racer;
		this.maxLifeSpan = maxLifeSpan;
	}
	
	public void priceCategory(){
		if(this.racer){
			System.out.println(" That's some expensive horse you got there !!!");
		}else{
			System.out.println(" Regular horse still cute ");
		}
	}

	public void funFacts(){
		if (this.type.equals("Arabian")){
			System.out.println(" Did you know that they weigh  between 800 to 1000 lbs ? ");
		}else {
			System.out.println(" Horses have a nearly 360-degree field of vision ");
		}
	}
	
	//setters 
	
	public void setMaxLifeSpan(int maxLifeSpan){
		this.maxLifeSpan = maxLifeSpan;
	}
	
	//getters
	
	public String getType(){
		return this.type;
	}
	
	public boolean getRacer(){
		return this.racer;
	}
	
	public int getMaxLifeSpan(){
		return this.maxLifeSpan;
	}
}

